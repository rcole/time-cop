#include <string>
#include "kid_manager.h"
#include "stopwatch.h"

#ifndef TIME_MANAGER_H
#define TIME_MANAGER_H

class TimeManager 
{
  public:
    TimeManager();
    void update_clock();
    void start_time();
    void stop_time();
    void set_edit_mode(bool enabled);
    void save_time();
    void start_refresh_loop();
    unsigned int set_edit_time_buffer(bool direction);
    unsigned int get_remaining_time_sec();
    bool get_timer_state(){ return timer_state; }
    bool get_edit_state(){ return edit_enabled; }
  private:
    const char * kid_name;
    bool timer_state;
    bool edit_enabled;
    unsigned int remaining_time_sec;
    unsigned int last_clock_millis;
    unsigned int remaining_time_buffer_sec;
    unsigned int edit_buffer_sec;
    unsigned int edit_time_buffer;
    KidManager kid_manager;
    Stopwatch stopwatch;
    std::string format_seconds();
};

#endif
