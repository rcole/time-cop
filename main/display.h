#ifndef DISPLAY_H
#define DISPLAY_H

class Display 
{
  public:
    Display();
    Display(int din, int cs, int clk);
    void set_display(int display_data);
    void play_animation(int animation_name);
  private:
    int din_pin;
    int cs_pin;
    int clk_pin;
};

#endif
