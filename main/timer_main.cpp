#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "app_manager.h"

extern "C" void app_main(void)
{
    AppManager appmanager = AppManager();

    appmanager.init_app();

    for (;;)
    {
        /* code */
        appmanager.run_loop();
        vTaskDelay(1);
    }
    
}