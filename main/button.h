#include "driver/gpio.h"

#ifndef BUTTON_H
#define BUTTON_H

class Button 
{
  public:
    Button(gpio_num_t pin, int hold_delay = 0);
    bool get_pressed(){ return is_pressed; }
    bool get_held(){ return is_held; }
    bool is_active();
    bool is_active_held();
    void check();
  private:
    const gpio_num_t button_pin;
    int hold_delay_millis;
    int debounce_delay_millis;
    bool is_pressed;
    bool is_held;
    unsigned int last_button_millis;
};

#endif
