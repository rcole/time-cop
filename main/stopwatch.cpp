#include <time.h>
#include <math.h>
#include <stdio.h>

#include "stopwatch.h"

Stopwatch::Stopwatch()
{ 
  is_running = false;
  // set both to current time initially
  last_start = time(NULL);
  last_stop = time(NULL);
  printf("Init stopwatch main\n");
}

int Stopwatch::get_elapsed_seconds()
{
  if (is_running)
  {
    return difftime(get_time(), last_start);
  }
  else
  {
    int seconds = difftime(last_stop, last_start);
    return seconds ? seconds > 0 : 0;
  }
}

time_t Stopwatch::get_time()
{
  return time(NULL);
}

void Stopwatch::start()
{
  is_running = true;
  last_start = get_time();
}

void Stopwatch::pause()
{
  is_running = false;
  last_stop = get_time();
}