#include "driver/gpio.h"

#include "button.h"
#include "led.h"
// #include "display.h"
// #include "encoder.h"
#include "time_manager.h"

#include "app_manager.h"

AppManager::AppManager()
{
}

void AppManager::init_app()
{
  running = false;
  last_run_press = false;
  last_edit_press = false;
  editing = false;
  edit_buffer_sec = 0;
  TimeManager active_time_manager();
  configure_inputs();
  configure_outputs();
}

void AppManager::configure_inputs()
{
  gpio_num_t START_STOP_PIN = GPIO_NUM_4;
  gpio_num_t EDIT_PIN = GPIO_NUM_5;
  // gpio_num_t ENCODER_1_PIN = GPIO_NUM_15;
  // gpio_num_t ENCODER_2_PIN = GPIO_NUM_2;
  // gpio_num_t KID_SELECT_1_PIN = GPIO_NUM_25;
  // gpio_num_t KID_SELECT_2_PIN = GPIO_NUM_26;

  start_stop_btn = new Button(START_STOP_PIN, 3);
  edit_mode_btn = new Button(EDIT_PIN, 2);

  // Encoder edit_mode_encoder(ENCODER_1_PIN, ENCODER_2_PIN);
  // TODO add switch
}

void AppManager::configure_outputs()
{
  gpio_num_t LED_RUNNING_LIGHT_PIN = GPIO_NUM_23;
  // gpio_num_t DISPLAY_DIN_PIN = GPIO_NUM_13;
  // gpio_num_t DISPLAY_CS_PIN = GPIO_NUM_12;
  // gpio_num_t DISPLAY_CLK_PIN = GPIO_NUM_14;

  running_light = new Led(LED_RUNNING_LIGHT_PIN);
  // Display display(DISPLAY_DIN_PIN, DISPLAY_CS_PIN, DISPLAY_CLK_PIN);
}

void AppManager::run_loop()
{
  start_stop_btn->check();
  edit_mode_btn->check();

  if (running)
  {
    active_time_manager.update_clock();
  }

  bool pressed = start_stop_btn->get_pressed();
  if (pressed != last_run_press)
  {
    last_run_press = pressed;
    if (pressed)
    {
      last_run_press = pressed;
      start_stop_handler();
    }
  }

  if (!running)
  {
    bool held = edit_mode_btn->get_held();
    if (held != last_edit_press)
    {
      last_edit_press = held;
      if (pressed)
      {
        last_edit_press = held;
        start_stop_handler();
      }
    }
  }
}

void AppManager::start_stop_handler()
{
  running = !running;
  running_light->set_state(running);
  printf("start stop handler: %d\n", running);
  if (running)
  {
    active_time_manager.start_time();
  }
  else
  {
    active_time_manager.stop_time();
  }
}

void AppManager::edit_mode_handler(bool button_state)
{
  editing = button_state;
  // running_light.set_state(running);
  if (editing)
  {
    // enter edit mode
  }
  else
  {
    // exit edit mode
  }
}

void AppManager::edit_value_handler(bool direction)
{
}

void AppManager::load_kid(const char *name)
{
  // TimeManager active_time_manager(name);
}
