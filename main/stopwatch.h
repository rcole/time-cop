#include <time.h>
#include <stdbool.h>

#ifndef STOPWATCH_H
#define STOPWATCH_H

class Stopwatch
{
public:
    Stopwatch();
    void start();
    void pause();
    int get_elapsed_seconds();
    bool get_is_running() { return is_running;}
    time_t get_last_start() { return last_start; }
    time_t get_last_stop() { return last_stop; }
private:
    bool is_running;
    time_t last_start;
    time_t last_stop;
    time_t get_time();
};

#endif