#ifndef LED_H
#define LED_H

class Led 
{
  public:
    Led(gpio_num_t pin);
    void set_state(bool state);
    bool get_state(){ return led_state; }
  private:
    unsigned int led_pin;
    bool led_state;
};

#endif
