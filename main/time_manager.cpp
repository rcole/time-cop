#include <stdio.h>
#include <string>
#include <esp_log.h>
#include <ios> 	
#include <iomanip>
#include <sstream>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "sdkconfig.h"

#include "stopwatch.h"
// #include "kid_manager.h"

#include "time_manager.h"

TimeManager::TimeManager()
{
  // kid_name = name;
  // KidManager kid_manager(name, false);
  Stopwatch stopwatch;
  timer_state = false;
  remaining_time_sec = 600;
  last_clock_millis = 0;
  remaining_time_buffer_sec = 0;
  edit_time_buffer = 0;
}
void TimeManager::start_time()
{
  timer_state = true;
  stopwatch.start();
}

void TimeManager::stop_time()
{
  timer_state = false;
  remaining_time_sec = get_remaining_time_sec();
  stopwatch.pause();
  printf("remaining_time_sec: %d\n", remaining_time_sec);
  printf("get_remaining_time_sec: %d\n", get_remaining_time_sec());
}

unsigned int TimeManager::get_remaining_time_sec()
{
  return remaining_time_sec - stopwatch.get_elapsed_seconds();
}

void TimeManager::update_clock()
{ 
  int now = esp_log_timestamp();
  if(now - last_clock_millis >= 1000){
    last_clock_millis = esp_log_timestamp();
    printf("Updating display :: %s\n", format_seconds().c_str()); //note the use of c_str
  }
}

std::string TimeManager::format_seconds()
{
  int seconds, minutes;
  int remaining = get_remaining_time_sec();
  minutes = int(remaining / 60);
  seconds = int(remaining%60);

  std::ostringstream min_out, sec_out;
  min_out << std::internal << std::setfill('0') << std::setw(2) << minutes;
  sec_out << std::internal << std::setfill('0') << std::setw(2) << seconds;
  return min_out.str() + ":" + sec_out.str();
}

void TimeManager::set_edit_mode(bool enabled)
{
  if(edit_enabled && !enabled){
    // setting from on to off, save buffer state
    edit_time_buffer = 0;
  }
  edit_enabled = enabled;
}

unsigned int TimeManager::set_edit_time_buffer(bool direction)
{
  if(direction){
    edit_buffer_sec++;
  } else {

    edit_buffer_sec--;
  }
  return edit_buffer_sec;
}

void TimeManager::save_time()
{
  kid_manager.save_kid_file(1);
}
