#ifndef KID_MANAGER_H
#define KID_MANAGER_H

class KidManager 
{
  public:
    KidManager();
    KidManager(char16_t name, bool time_rollover = false);
    void init_kid_file();
    void save_kid_file(int kid_file);
    void load_kid_file();
    int get_kid_day_config(int day);
  private:
    wchar_t kid_name;
    wchar_t kid_file_path;
    int daily_limits_mins[7];
};

#endif
