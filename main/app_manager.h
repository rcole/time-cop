#include "button.h"
#include "led.h"
// #include "display.h"
// #include "encoder.h"
// #include "kid_manager.h"
#include "time_manager.h"

#ifndef APP_MANAGER_H
#define APP_MANAGER_H

class AppManager
{
  public:
    AppManager();
    void init_app();
    void load_kid(const char * name);
    void run_loop();
  private:
    const char * kids[2];
    int edit_buffer_sec;
    bool last_run_press;
    bool last_edit_press;
    bool running;
    bool editing;
    Led* running_light;
    Button* start_stop_btn;
    Button* edit_mode_btn;
    // Encoder edit_mode_encoder;
    // Display display;
    TimeManager active_time_manager;
    void configure_inputs();
    void configure_outputs();
    void start_stop_handler();
    void edit_mode_handler(bool button_state);
    void edit_value_handler(bool direction);
};

#endif
