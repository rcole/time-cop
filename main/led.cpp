#include <stdio.h>
#include "driver/gpio.h"

#include "led.h"

Led::Led(gpio_num_t pin)
    : led_pin(pin),
      led_state(false)
{
  gpio_set_direction((gpio_num_t)led_pin, GPIO_MODE_OUTPUT);
  gpio_set_level((gpio_num_t)led_pin, 0);
}

void Led::set_state(bool state)
{
  led_state = state;
  printf("led pin: %d \n", (gpio_num_t)led_pin);
  printf("led level: %d \n", led_state);
  gpio_set_level((gpio_num_t)led_pin, led_state);
}
