#ifndef ENCODER_H
#define ENCODER_H

class Encoder 
{
  public:
    Encoder();
    Encoder(int pin_1, int pin_2);
  private:
    int encoder_pin_1;
    int encoder_pin_2;
    void handle_change();
};

#endif
