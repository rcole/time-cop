#include <stdio.h>
#include <esp_log.h>
#include "driver/gpio.h"

#include "button.h"

Button::Button(gpio_num_t pin, int hold_delay)
    : button_pin(pin),
      hold_delay_millis(hold_delay),
      debounce_delay_millis(50),
      is_pressed(false),
      is_held(false)
{

  gpio_set_direction((gpio_num_t)button_pin, GPIO_MODE_INPUT);
  gpio_set_pull_mode((gpio_num_t)button_pin, GPIO_PULLDOWN_ONLY);
  gpio_intr_disable((gpio_num_t)button_pin);
}

void Button::check()
{
  int level = gpio_get_level((gpio_num_t)button_pin);
  if (level == 0)
  {
    is_pressed = false;
    is_held = false;
    return;
  }

  int current_millis = esp_log_timestamp();

  if (current_millis - last_button_millis > 100 && !!level != is_pressed)
  {
    printf("Debounced Press \n");
    last_button_millis = esp_log_timestamp();
    is_pressed = true;
  }

  if (is_pressed && current_millis - last_button_millis > 2000 && !!level != is_held)
  {
    printf("Held Press \n");
    is_held = true;
  }
}
